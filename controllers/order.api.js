var express = require("express"),
 router = express.Router(),
 order = require("../models/order.js");


 // creating express server
 
router.get("/", function(req, res) {
  order.find({}, function(err, data) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  if (err) {
  res.send("error");
  return;
  }
  res.send(data);
  });
}).get("/:id", function(req, res) {
 var id = req.params.id;
 order.find({ _id: id }, function(err, data) {
 if (err) {
 res.send("error");
 return;
 }
 res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
 res.send(data[0]);
 });
}).post("/", function(req, res) {


 var obj = req.body;
 var model = new order(obj);
 model.save(function(err) {
 if (err) {

 res.send("error");
 return;

 }
 res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
 res.send("created");
 });
}).put("/:id", function(req, res) {
   
   


 var id = req.params.id;
 var obj = req.body;
  console.log(obj);
 order.findByIdAndUpdate(id, {ordernumber: obj.ordernumber, ordername: obj.ordername, orderstatus: obj.orderstatus, clientname: obj.clientname,clientaddress: obj.clientaddress,phonenuber:obj.phonenuber,driver:obj.driver},
function(err) {
 if (err) {
 res.send("error");
 return;
 }
 res.send("updated");
 });
}).delete("/:id", function(req, res) {
 var id = req.params.id;
 order.findByIdAndRemove(id, function(err) {
 if (err) {
 res.send("error");
 return;
 }
 res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
 res.send("deleted");
 });
});
 
module.exports = router;
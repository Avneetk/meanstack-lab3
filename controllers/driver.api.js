var express = require("express"),
 router = express.Router(),
 driver = require("../models/driver.js");


 // creating express server
 
router.get("/", function(req, res) {
  driver.find({}, function(err, data) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  if (err) {
  res.send("error");
  return;
  }
  res.send(data);
  });
}).get("/:id", function(req, res) {
 var id = req.params.id;
 driver.find({ _id: id }, function(err, data) {
 if (err) {
 res.send("error");
 return;
 }
 res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
 res.send(data[0]);
 });
}).post("/", function(req, res) {


 var obj = req.body;
 var model = new driver(obj);
 model.save(function(err) {
 if (err) {

 res.send("error");
 return;

 }
 res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
 res.send("created");
 });
}).put("/:id", function(req, res) {
   
   


 var id = req.params.id;
 var obj = req.body;
  console.log(obj);
 driver.findByIdAndUpdate(id, {name: obj.name, phone: obj.phone, address: obj.address},
function(err) {
 if (err) {
 res.send("error");
 return;
 }
 res.send("updated");
 });
}).delete("/:id", function(req, res) {
 var id = req.params.id;
 driver.findByIdAndRemove(id, function(err) {
 if (err) {
 res.send("error");
 return;
 }
 res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
 res.send("deleted");
 });
});
 
module.exports = router;
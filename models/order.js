var mongoose = require("mongoose"),
 Schema = mongoose.Schema,
 objectId = mongoose.Schema.ObjectId;

 //========= user details created in the schema  ==========
 
var orderSchema = new Schema({

 _id: { type: objectId, auto: true },

 ordernumber: { type: String, required: true },
 ordername:{ type: String, required: true },
 orderstatus:{ type: String, required: true },
 clientname:{ type: String, required: true },
 clientaddress:{ type: String, required: true },
 phonenuber:{ type: String, required: true },
 driver:{ type: String, required: true },

}, {
 versionKey: false
});
 

var order = mongoose.model('orders', orderSchema);
 
module.exports = order;
var mongoose = require("mongoose"),
 Schema = mongoose.Schema,
 objectId = mongoose.Schema.ObjectId;

 //========= user details created in the schema  ==========
 
var orderSchema = new Schema({

 _id: { type: objectId, auto: true },

 name: { type: String, required: true },
 phone:{ type: String, required: true },
 address:{ type: String, required: true },
 

}, {
 versionKey: false
});
 

var driver = mongoose.model('drivers', orderSchema);
 
module.exports = driver;
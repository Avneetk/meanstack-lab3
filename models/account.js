var mongoose = require("mongoose"),
 Schema = mongoose.Schema,
 objectId = mongoose.Schema.ObjectId;

 //========= user details created in the schema  ==========
 
var orderSchema = new Schema({

 _id: { type: objectId, auto: true },

 name: { type: String, required: true },
 email:{ type: String, required: true },
 password:{ type: String, required: true },
 phone:{ type: Number, required: true },
 usertype:{ type: String, required: false }
 

}, {
 versionKey: false
});
 

var account = mongoose.model('accounts', orderSchema);
 
module.exports = account;
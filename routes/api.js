 var express = require('express'),
 router = express.Router();
 
//routes for user api
router.use("/user", require("../controllers/user.api"));
router.use("/order", require("../controllers/order.api"));
router.use("/driver", require("../controllers/driver.api"));
router.use("/account", require("../controllers/account.api"));


 
//add here other api routes
 
module.exports = router;